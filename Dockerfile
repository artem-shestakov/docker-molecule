ARG DOCKER_VERSION=26.0.1-cli
FROM docker:${DOCKER_VERSION}
RUN apk add --no-cache python3 python3-dev py3-pip git rsync
RUN python3 -m pip install --upgrade pip --break-system-packages
RUN pip3 install molecule "molecule-plugins[docker]" pytest pytest-testinfra --break-system-packages
RUN ansible-galaxy collection install community.docker ansible.posix
